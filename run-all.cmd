call xalan -in "%XMI-FILENAME%.xmi" -xsl %TO-CANONICAL-XMI-HOME%\to-canonical-xmi-step-1.xslt -out "%XMI-FILENAME%_step-1.xmi"
pause
call xalan -in "%XMI-FILENAME%_step-1.xmi" -xsl %TO-CANONICAL-XMI-HOME%\to-canonical-xmi-step-2.xslt -out "%XMI-FILENAME%_step-2.xmi"
pause
call xalan -in "%XMI-FILENAME%_step-2.xmi" -xsl %TO-CANONICAL-XMI-HOME%\to-canonical-xmi-step-3.xslt -out "%XMI-FILENAME%_step-3.xmi" -param UniqueNames no
call xalan -in "%XMI-FILENAME%_step-2.xmi" -xsl %TO-CANONICAL-XMI-HOME%\to-canonical-xmi-step-3.xslt -out "%XMI-FILENAME%_step-3-unique-names.xmi" -param UniqueNames yes
pause
call xalan -in "%XMI-FILENAME%_step-3.xmi" -xsl %TO-CANONICAL-XMI-HOME%\to-canonical-xmi-step-4.xslt -out "%XMI-FILENAME%_step-4.xmi"
call xalan -in "%XMI-FILENAME%_step-3-unique-names.xmi" -xsl %TO-CANONICAL-XMI-HOME%\to-canonical-xmi-step-4.xslt -out "%XMI-FILENAME%_step-4-unique-names.xmi"
call xalan -in "%XMI-FILENAME%_step-4-unique-names.xmi" -xsl %TO-CANONICAL-XMI-HOME%\to-canonical-xmi-validate-ids.xslt -out "%XMI-FILENAME%_step-4-unique-names-validate-ids.log"
pause
call saxon -xsl:%TO-CANONICAL-XMI-HOME%\format.xslt -s:"%XMI-FILENAME%_step-4.xmi" -o:"%XMI-BASE-FILENAME%_canonical.xmi"
call saxon -xsl:%TO-CANONICAL-XMI-HOME%\format.xslt -s:"%XMI-FILENAME%_step-4-unique-names.xmi" -o:"%XMI-BASE-FILENAME%_canonical-unique-names.xmi"
pause
copy "%XMI-BASE-FILENAME%_canonical-unique-names.xmi" "%XMI-BASE-FILENAME%_canonical-unique-names-eclipse.xmi"
sed -i "15,$s|http://www.omg.org/spec/UML/20131001/StandardProfile|http://www.eclipse.org/uml2/5.0.0/UML/Profile/Standard|g" "%XMI-BASE-FILENAME%_canonical-unique-names-eclipse.xmi"
sed -i "15,$s|http://www.omg.org/spec/UML/20131001|http://www.eclipse.org/uml2/5.0.0/UML|g" "%XMI-BASE-FILENAME%_canonical-unique-names-eclipse.xmi"