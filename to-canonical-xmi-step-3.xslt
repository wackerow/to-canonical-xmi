<?xml version="1.0" encoding="UTF-8"?>
<!--
Transformation to Canonical XMI according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (short CX)
	https://www.omg.org/spec/XMI/2.5.1/PDF
	from XMI export flavor of Enterprise Architect 14.1, exporter version 6.5
		Publish / Other Formats
			XML Type: UML 2.4.1 (XMI 2.4.2)
			"Export Diagrams" not checked
			"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XML.
CONFIGURE / Resources / Stylesheets (right-click)

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-3:
  create xmi:id and xmi:uuid according to CX

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:uml="http://www.omg.org/spec/UML/20131001" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org" xmlns:StandardProfile="http://www.omg.org/spec/UML/20131001/StandardProfile" extension-element-prefixes="exslt msxsl xalan">
	<!--
Set explicit encoding of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
-->
	<xsl:strip-space elements="StandardProfile:*"/>
	<!--
EA: Set ModelName to name of first package in wrapper package with name "Model" -->
	<xsl:param name="ModelName" select="/xmi:XMI/uml:Model[ @xmi:type='uml:Model' ][ @name='EA_Model' ]/packagedElement[ @xmi:type='uml:Package' ][ @name='Model' ]/packagedElement[ @xmi:type='uml:Package' ]/@name"/>
	<!--
Set DDI4_XMI_URI_Root -->
	<xsl:param name="DDI4_XMI_URI_Root" select=" 'http://ddialliance.org/Specification/DDI-CDI/1.0/XMI/' "/>
	<!--
Set PrimitiveTypes URI -->
	<xsl:param name="PrimitiveTypesURI" select=" 'http://www.omg.org/spec/UML/20131001/PrimitiveTypes.xmi' "/>
	<!--
Option for unique association names -->
	<xsl:param name="UniqueNames" select=" 'no' "/>
	<!--
Set character for word separator -->
	<xsl:param name="WordSeparator" select=" '_' "/>
	<!--
for translate -->
	<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
	<!--
Key for all IDs -->
	<xsl:key name="id" match="//*[@xmi:id]" use="@xmi:id"/>
	<!--
Key for all idrefs -->
	<xsl:key name="idref" match="//*[@xmi:idref]" use="@xmi:idref"/>
	<!--
 -->
	<xsl:key name="ownedAttribute" match="//ownedAttribute" use="@xmi:id"/>
	<xsl:key name="ownedEnd" match="//ownedEnd" use="@xmi:id"/>
	<!--
Entry point -->
	<xsl:template match="/">
		<xsl:apply-templates mode="step-3" select="node()"/>
	</xsl:template>
	<!--
XMI -->
	<xsl:template match="xmi:XMI">
		<xsl:apply-templates mode="step-3" select="uml:Model"/>
		<xsl:apply-templates mode="step-3" select="StandardProfile:Derive | StandardProfile:Refine | StandardProfile:Trace">
			<xsl:sort select="base_Abstraction/@xmi:idref"/>
		</xsl:apply-templates>
	</xsl:template>
	<!--
Process nodes -->
	<xsl:template mode="step-3" match="node()">
		<xsl:copy>
			<xsl:apply-templates mode="step-3" select="@*"/>
			<xsl:apply-templates mode="step-3" select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process attributes -->
	<xsl:template mode="step-3" match="@*">
		<xsl:copy/>
	</xsl:template>
	<!--
Process attribute id -->
	<xsl:template mode="step-3" match="@xmi:id">
		<xsl:variable name="IdValue">
			<xsl:call-template name="CreateID">
				<xsl:with-param name="Node" select=".."/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="UuidValue">
			<xsl:call-template name="CreateUUID">
				<xsl:with-param name="String" select="$IdValue"/>
			</xsl:call-template>
		</xsl:variable>
		<!--
		<xsl:variable name="UuidValue" select="concat( $DDI4_XMI_URI_Root, translate( $IdValue, '-', '/' ) )"/>
		-->
		<xsl:attribute name="xmi:id">
			<xsl:value-of select="$IdValue"/>
		</xsl:attribute>
		<xsl:attribute name="xmi:uuid">
			<xsl:value-of select="$UuidValue"/>
		</xsl:attribute>
	</xsl:template>
	<!--
Process attribute idref -->
	<xsl:template mode="step-3" match="@xmi:idref">
		<xsl:variable name="IdValue">
			<xsl:call-template name="CreateID">
				<xsl:with-param name="Node" select="key( 'id', . )"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:attribute name="xmi:idref">
			<xsl:value-of select="$IdValue"/>
		</xsl:attribute>
		<xsl:if test="$IdValue = '' ">
			<xsl:message>
				<xsl:text>Reference has no xmi:id: </xsl:text>
				<xsl:value-of select="."/>
				<xsl:text>. XPath is: </xsl:text>
				<xsl:for-each select="ancestor::*">
					<xsl:text>/</xsl:text>
					<xsl:value-of select="name(.)"/>
				</xsl:for-each>
			</xsl:message>
		</xsl:if>
	</xsl:template>
	<!--
Process StandardProfile stereotypes
-->
	<xsl:template mode="step-3" match="StandardProfile:Derive | StandardProfile:Refine | StandardProfile:Trace">
		<xsl:variable name="Id">
			<xsl:text>_-Abstraction-</xsl:text>
			<xsl:value-of select="local-name()"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="generate-id()"/>
		</xsl:variable>
		<xsl:copy>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$Id"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="/xmi:XMI/uml:Model/name"/>
				<xsl:value-of select="$Id"/>
			</xsl:attribute>
			<xsl:apply-templates mode="step-3" select="@xmi:type"/>
			<xsl:apply-templates mode="step-3" select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process abstraction
-->
	<xsl:template mode="step-3" match="packagedElement[@xmi:type='uml:Abstraction']">
		<xsl:copy>
			<xsl:apply-templates mode="step-3" select="@*"/>
			<xsl:apply-templates mode="step-3" select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process abstraction name
-->
	<xsl:template mode="step-3" match="name[ancestor::packagedElement[@xmi:type='uml:Abstraction'] ]">
		<xsl:choose>
			<xsl:when test=" $UniqueNames='yes' ">
				<xsl:call-template name="AbstractionName">
					<xsl:with-param name="AbstractionName" select="."/>
					<xsl:with-param name="AbstractionNode" select=".."/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates mode="step-3" select="node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--
Create unique abstraction name in model
-->
	<xsl:template name="AbstractionName">
		<xsl:param name="AbstractionName"/>
		<xsl:param name="AbstractionNode"/>
		<xsl:for-each select="$AbstractionNode">
			<xsl:variable name="SupplierName" select="key( 'id', supplier/@xmi:idref)/name"/>
			<xsl:variable name="ClientName" select="key( 'id', client/@xmi:idref)/name"/>
			<xsl:element name="name">
				<xsl:value-of select="$ClientName"/>
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$AbstractionName"/>
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$SupplierName"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!--
Process association
-->
	<xsl:template mode="step-3" match="packagedElement[@xmi:type='uml:Association']">
		<xsl:copy>
			<xsl:apply-templates mode="step-3" select="@*"/>
			<!-- add name if it does not exist -->
			<xsl:if test="not(name)">
				<xsl:call-template name="AssociationName">
					<xsl:with-param name="AssociationNode" select="."/>
					<xsl:with-param name="AssociationName" select=" 'TODOrelatesTo' "/>
				</xsl:call-template>
			</xsl:if>
			<xsl:apply-templates mode="step-3" select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process association name
-->
	<xsl:template mode="step-3" match="name[ancestor::packagedElement[@xmi:type='uml:Association'] ]">
		<xsl:choose>
			<xsl:when test=" $UniqueNames='yes' ">
				<xsl:call-template name="AssociationName">
					<xsl:with-param name="AssociationNode" select=".."/>
					<xsl:with-param name="AssociationName" select="."/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates mode="step-3" select="node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--
Create unique association name in model
-->
	<xsl:template name="AssociationName">
		<xsl:param name="AssociationNode"/>
		<xsl:param name="AssociationName"/>
		<xsl:for-each select="$AssociationNode">
			<!-- association end owned by class, only one memberEnd reference will be found -->
			<xsl:variable name="SourceId" select="key( 'ownedEnd', memberEnd[1]/@xmi:idref | memberEnd[2]/@xmi:idref )/type/@xmi:idref"/>
			<xsl:variable name="SourceClassName" select="key( 'id', $SourceId )/name"/>
			<!-- association end owned by association, only one memberEnd reference will be found -->
			<xsl:variable name="TargetId" select="key( 'ownedAttribute', memberEnd[1]/@xmi:idref | memberEnd[2]/@xmi:idref )/type/@xmi:idref"/>
			<xsl:variable name="TargetClassName" select="key( 'id', $TargetId )/name"/>
			<name>
				<xsl:value-of select="$SourceClassName"/>
				<xsl:value-of select="$WordSeparator"/>
				<xsl:value-of select="$AssociationName"/>
				<xsl:value-of select="$WordSeparator"/>
				<xsl:value-of select="$TargetClassName"/>
			</name>
		</xsl:for-each>
	</xsl:template>
	<!--
Create unique abstraction name in model
-->
	<!--	<xsl:template name="XXAbstractionName">
		<xsl:param name="AbstractionNode"/>
		<xsl:param name="AbstractionName"/>
		<xsl:for-each select="$AbstractionNode">

			<name>
				<xsl:value-of select="$SourceClassName"/>
				<xsl:value-of select="$WordSeparator"/>
				<xsl:value-of select="$AbstractionName"/>
				<xsl:value-of select="$WordSeparator"/>
				<xsl:value-of select="$TargetClassName"/>
			</name>
		</xsl:for-each>
	</xsl:template>
-->
	<!--
Create id according CX -->
	<xsl:template name="CreateID">
		<xsl:param name="Node"/>
		<!-- set context -->
		<xsl:for-each select="$Node">
			<!-- without root XMI node -->
			<xsl:for-each select="ancestor-or-self::*[parent::*]">
				<xsl:variable name="Name" select="name"/>
				<xsl:choose>
					<xsl:when test="name">
						<xsl:choose>
							<!-- if abstraction -->
							<xsl:when test="@xmi:type='uml:Abstraction'">
								<xsl:call-template name="AbstractionName">
									<xsl:with-param name="AbstractionName" select="name"/>
									<xsl:with-param name="AbstractionNode" select="."/>
								</xsl:call-template>
							</xsl:when>
							<!-- if association -->
							<xsl:when test="@xmi:type='uml:Association'">
								<xsl:call-template name="AssociationName">
									<xsl:with-param name="AssociationNode" select="."/>
									<xsl:with-param name="AssociationName" select="name"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="name"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<!-- without name -->
					<xsl:otherwise>
						<xsl:choose>
							<!-- if ownedAttribute without name (association end) -->
							<xsl:when test=" local-name()='ownedAttribute' and count(../ownedAttribute[not(name)]) > 1 ">
								<xsl:value-of select="local-name()"/>
								<xsl:variable name="SequenceNumber" select="count( preceding-sibling::ownedAttribute[not(name)] ) + 1"/>
								<!-- add sequence number -->
								<xsl:value-of select="$WordSeparator"/>
								<xsl:value-of select="$SequenceNumber"/>
							</xsl:when>
							<!-- if ownedEnd without name (association end) (?does this happen?) -->
							<xsl:when test=" local-name()='ownedEnd' and count(../ownedEnd[not(name)]) > 1 ">
								<xsl:value-of select="local-name()"/>
								<xsl:variable name="SequenceNumber" select="count( preceding-sibling::ownedEnd[not(name)] ) + 1"/>
								<!-- add sequence number -->
								<xsl:value-of select="$WordSeparator"/>
								<xsl:value-of select="$SequenceNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="local-name()"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="not( position()=last() )">
					<xsl:text>-</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
	<!--
Create short ID -->
	<xsl:template name="CreateUUID">
		<xsl:param name="String"/>
		<xsl:variable name="Substring" select="substring-after($String, '-')"/>
		<xsl:variable name="StartCharacter" select="substring($Substring, 1, 1)"/>
		<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
		<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:choose>
			<xsl:when test="contains($String, '-') and $StartCharacter = translate($StartCharacter, $lowercase, $uppercase)">
				<xsl:call-template name="CreateUUID">
					<xsl:with-param name="String" select="$Substring"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($DDI4_XMI_URI_Root, '#', $String)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--
Determine name of association end -->
	<xsl:template name="ClassName">
		<xsl:param name="Reference"/>
		<xsl:variable name="Node" select="key( 'id', $Reference)"/>
		<xsl:value-of select="key( 'id', $Node/type/@xmi:idref)/name"/>
	</xsl:template>
</xsl:stylesheet>