# Transforming XMI to Canonical XMI

*Joachim Wackerow, 2022-03-15*

XMI is used by UML tools in many variants. Canonical XMI constitutes a specific constrained format of XMI that supports interoperability between UML tools.

This tool transforms XMI files of UML class models that conform to the UML Class Model Interoperable Subset (UCMIS) into Canonical XMI.

The transformation is mainly tested for Enterprise Architect XMI but should also work for other XMI variants. The resulting Canonical XMI should be importable into common UML tools. Some successful tests have been performed with:

- Open Source
    - Papyrus for UML Version 3.3.0 in Eclipse Modeling Tools Oxygen.3 Release (4.7.3)
    - Umbrello UML Modeller Version 2.24.3
    - UML Designer Version 8.0.0 (Eclipse-based)
- Commercial
    - Astah Professional Version 7.2.0
    - Enterprise Architect Version 13.5 and 14.0, free viewer
    - IBM Rational Rhapsody Version 8.3
    - MagicDraw Version 18.5 and 19.0, free viewer available
    - UModel Enterprise Edition Version 2018 rel. 2 sp1
    - Visual Paradigm Version 15.0

## Transforming an XMI variant into Canonical XMI

The transformation is done by a set of XSLT 1.0 stylesheets. Transformation is done by the XSL processor Xalan. The final formatting is done by the XSL processor Saxon to get nicely formatted XML files.
The GNU stream editor sed is used as well for editing files in batch mode.

Following files are required:

- run.cmd
- run-all.cmd
- to-canonical-xmi-step-1.xslt
- to-canonical-xmi-step-2.xslt
- to-canonical-xmi-step-3.xslt
- to-canonical-xmi-step-4.xslt
- to-canonical-xmi-validate-ids.xslt

The file `run-all.cmd` expects that XMI source files have the extension '.xmi'.
The file `run.cmd` is here customized for an Enterprise Architect XMI filename which ends with '_pub_EAXMI'.

Customize the variables in the file `run.cmd`. 

```console
set TO-CANONICAL-XMI-HOME=E:\Tools\to-canonical-xmi
set XMI-BASE-FILENAME=E:\DDI\CDI\DDI-CDI_2022-03-09\DDI-CDI_2022-03-09
set FILENAME-POSTFIX=_pub_EAXMI
set MODEL-ROOT=DDICDIModels
```

TO-CANONICAL-XMI-HOME: home of the tool files.
XMI-BASE-FILENAME: base filename without '_pub_EAXMI.xmi' (here used postfix for the Enterprise Architect XMI file)
MODEL-ROOT: the root of the UML model.

Run the series of XSLT stylesheets in a Windows console with `run.cmd`.

## Final Steps after Transformation

Check the console and the validation output on errors.

Example log file of validation of identifiers: `DDI-CDI_2022-03-09_pub_EAXMI_step-4-unique-names-validate-ids.log`.

The messages belong to three different tests.

- Test if XMI identifiers are referenced
    - Information: It is not expected that these XMI identifiers are referenced.
    - Warning: These items might have no definitions. They should be checked manually.
- Test if XMI references have related XMI identifiers
    - There should be no error messages.
- Test if XMI identifiers are unique
    - There should be no error messages.

Two canonical XMI files are created, one with the original (non-unique) association names and one with unique association names. The unique association names are based on the original names with source and target class names added.

Example resulting Canonical XMI files:

- `DDI-CDI_2022-03-09.xmi`
- `DDI-CDI_2022-03-09-unique-names.xmi` (for usage in UML tools which expect unique association names)
- `DDI-CDI_2022-03-09-unique-names-eclipse.xmi` (for usage in Eclipse modeling tools)

## Namespaces

At the beginning of the files you will find information about the UML and XMI namespaces. This information is repeated below. The namespaces may have to be adapted to the UML tool used.

Namespaces are set for UML 2.5 and XMI 2.5.

For UML 2.4.1 and XMI 2.4.1:

- Change all occurences of '20131001' to '20100901'.

For Eclipse:

- Change the UML namespace from 'http://www.omg.org/spec/UML/20131001' to 'http://www.eclipse.org/uml2/5.0.0/UML'.
- Change the standard profile namespace from 'http://www.omg.org/spec/UML/20131001/StandardProfile' to 'http://www.eclipse.org/uml2/5.0.0/UML/Profile/Standard'.

## Details

The XSLT stylesheets are written for XSL 1.0 processors to be as flexible as possible with respect to the environment. Multiple tasks are performed by dedicated stylesheets.

The transformation to Canonical XMI is done according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (https://www.omg.org/spec/XMI/2.5.1/PDF).

- to-canonical-xmi-step-1
    - Ignoring namespaces, setting namespaces to UML 2.5 / XMI 2.5. This version is recommended because it is implemented in many UML tools.
    - Add XMI root if it doesn't exist.
- to-canonical-xmi-step-2
    - Add EA documentation.
    - Process only elements and properties from defined UML subset. Ignore other items.
    - Transform XML attributes to XML elements except xmi:id, xmi:uui, and xmi:type according CX B.2 5.
    - Remove items if they have default values.
    - Set correct URIs for UML primitives.
    - Order elements according to CX.
- to-canonical-xmi-step-3
    - Create xmi:id and xmi:uuid for each item.
- to-canonical-xmi-step-4
    - Sort nodes on same level with name by @xmi:uuid.
- to-canonical-xmi-validate-ids.xslt
    - Identifier validation tests.
