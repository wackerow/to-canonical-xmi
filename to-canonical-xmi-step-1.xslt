<?xml version="1.0" encoding="UTF-8"?>
<!--

Transformation to Canonical XMI according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (short CX)
	https://www.omg.org/spec/XMI/2.5.1/PDF

	from XMI export flavor of Enterprise Architect 14.1/15.0, exporter version 6.5, and other UML tools (list will follow)
		Select "Model" in project browswer
		Publish / Other Formats
			XML Type: UML 2.4.1 (XMI 2.4.2)
			"Export Diagrams" not checked
			"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XML.
Project must be loaded for import: Configure / Resources / Stylesheets (right-click)

The transformation expects a limited subset of UML class diagram items.

Covered UML Items:

Structural Items
  Model
  Package
  Class
  Property

Relationships
  Association
  Generalization

Data types
  DataType
  EnumerationLiteral
  Enumeration
  LiteralInteger
  LiteralString
  LiteralUnlimitedNatural
  PrimitiveType

Other
  Comment

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step 1: 
  Ignoring namespaces, setting namespaces to UML 2.4.1 / XMI 2.4.1. This version is recommended because it is implemented in many UML tools.
  Add XMI root if it doesn't exist.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uml="http://www.omg.org/spec/UML/20131001" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org" xmlns:StandardProfile="http://www.omg.org/spec/UML/20131001/StandardProfile" extension-element-prefixes="exslt msxsl xalan">
	<!--
Set explicit encoding with value of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
*** if used to do: remove namespace dependency 
Set tool name 
	<xsl:param name="Tool">
		<xsl:choose>
			<xsl:when test=" /xmi:XMI/xmi:Documentation/@exporter = 'Enterprise Architect' ">
				<xsl:text>EnterpriseArchitect</xsl:text>
			</xsl:when>
			<xsl:when test=" /xmi:XMI/uml:Model[2]/@name = 'RhapsodyStandardModel' ">
				<xsl:text>IBMRhapsody</xsl:text>
			</xsl:when>
			<xsl:when test=" /xmi:XMI/xmi:documentation/xmi:exporter = 'MagicDraw Clean XMI Exporter' ">
				<xsl:text>MagicDraw</xsl:text>
			</xsl:when>
			<xsl:when test="contains( /xmi:XMI/xmi:Documentation/exporter, 'Altova' )">
				<xsl:text>Umodel</xsl:text>
			</xsl:when>
			<xsl:when test="/xmi:XMI/uml:Model/xmi:Documentation/@exporter = 'Visual Paradigm' ">
				<xsl:text>VisualParadigm</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:param>
-->
	<!--
Entry point. -->
	<xsl:template match="/">
		<xsl:apply-templates mode="step-1" select="node()[ local-name()='XMI' or  local-name()='Model' ]"/>
	</xsl:template>
	<!--
process element xmi:XMI -->
	<xsl:template mode="step-1" match="node()[ local-name()='XMI' ]">
		<xmi:XMI xmlns:uml="http://www.omg.org/spec/UML/20131001" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='Documentation' or local-name()='documentation' ]"/>
			<!-- process only first element uml:Model like in case of IBM Rhapsody -->
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='Model' ][1]"/>
			<!-- process element xmi:Extension important in case of Enterprise Architect -->
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='Extension' ]"/>
			<!-- process profile elements including EA style ones -->
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='Derive' or local-name()='derive' or local-name()='Refine' or local-name()='refine' or local-name()='Trace' or local-name()='trace' ]"/>
		</xmi:XMI>
	</xsl:template>
	<!--
process element uml:Model -->
	<xsl:template mode="step-1" match="node()[ local-name()='Model' ]">
		<xsl:choose>
			<!-- if no XMI element like in case of Modelio -->
			<xsl:when test="count(ancestor::*) = 0">
				<xmi:XMI xmlns:uml="http://www.omg.org/spec/UML/20131001" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">
					<uml:Model>
						<xsl:apply-templates mode="step-1" select="@*"/>
						<xsl:if test="not( @*[ local-name() = 'id' ] )">
							<xsl:attribute name="xmi:id">
								<xsl:text>dummy</xsl:text>
							</xsl:attribute>
						</xsl:if>
						<xsl:apply-templates mode="step-1" select="node()"/>
					</uml:Model>
				</xmi:XMI>
			</xsl:when>
			<xsl:when test="@name = 'EA_Model' ">
				<uml:Model xmi:id="{packagedElement/@xmi:id}" name="{packagedElement/@name}">
					<!-- skip one level -->
					<xsl:apply-templates mode="step-1" select="packagedElement/node()"/>
				</uml:Model>
			</xsl:when>
			<xsl:otherwise>
				<uml:Model>
					<xsl:if test="not( @*[ local-name() = 'id' ] )">
						<xsl:attribute name="xmi:id">
							<xsl:text>dummy</xsl:text>
						</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates mode="step-1" select="@* | node()"/>
				</uml:Model>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--
process element xmi:Extension -->
	<xsl:template mode="step-1" match="node()[ local-name()='Documentation' or local-name()='Extension' ]">
		<xsl:variable name="QualifiedName" select="concat( 'xmi:', local-name() )"/>
		<xsl:element name="{$QualifiedName}">
			<xsl:apply-templates mode="step-1" select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	<!--	-->
	<!--
process profile elements including EA style (lowercase) -->
	<xsl:template mode="step-1" match="node()[ local-name()='Derive' or local-name()='derive' or local-name()='Refine' or local-name()='refine' or local-name()='Trace' or local-name()='trace' ]">
		<xsl:variable name="Name" select="local-name()"/>
		<xsl:variable name="Name2" select="concat( translate( substring($Name,1,1),
											'abcdefghijklmnopqrstuvwxyz',
											'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
										substring($Name,2,string-length($Name)-1) )"/>
		<xsl:variable name="QualifiedName" select="concat( 'StandardProfile:', $Name2 )"/>
		<xsl:element name="{$QualifiedName}">
			<xsl:choose>
				<xsl:when test="base_Abstraction">
					<xsl:apply-templates mode="step-1" select="@* | node()"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="base_Abstraction">
						<xsl:attribute name="xmi:idref">
							<!-- risky wild card -->
							<xsl:value-of select="@*"/>
						</xsl:attribute>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<!--
copy other elements -->
	<xsl:template mode="step-1" match="node()">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates mode="step-1" select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	<!--
skip attributes 
	<xsl:template mode="step-1" match="@*[ local-name()='version' or local-name()='Profile' ]"/>
-->
	<!--
copy attributes with prefix xmi -->
	<xsl:template mode="step-1" match="@*[ local-name()='id' or local-name()='uuid' or local-name()='type' or local-name()='idref' ]">
		<xsl:variable name="FullName" select="concat( 'xmi:', local-name() )"/>
		<xsl:attribute name="{$FullName}">
			<xsl:value-of select="."/>
		</xsl:attribute>
	</xsl:template>
	<!--
copy other attributes -->
	<xsl:template mode="step-1" match="@*">
		<xsl:attribute name="{local-name()}">
			<xsl:value-of select="."/>
		</xsl:attribute>
	</xsl:template>
	<!--
copy the rest of the nodes -->
	<xsl:template mode="step-1" match="comment() | text() | processing-instruction()">
		<xsl:copy/>
	</xsl:template>
</xsl:stylesheet>