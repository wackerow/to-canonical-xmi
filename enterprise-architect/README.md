# Exporting an Enterprise Architect Model to XMI
*Joachim Wackerow, 2022-03-15*

This is a description of how to export XMI from Enterprise Architect, which can be used as input for transformation into Canonical XMI. Please notice that the resulting XMI has an Enterprise Architect flavor that cannot be easily imported into other UML tools.

## Description

Open the Enterprise Architect file with Enterprise Architect (here version 15).

### Select the root package of the model in the project browser.

Example screenshot:

![](select-root.png)

### Export to Enterprise Architect XMI.

Enterprise Architect Menu:

1.  Publish / Publish As
	- Specify a filename
	- Select 'UML 2.5 (XMI 2.5.1)'
	- Specify options
		- Uncheck 'Export Diagrams'
		- Check 'Format XML Output'
		- Uncheck 'Write Log File'
		- 'use DTD' can't be unchecked.
		- Uncheck 'Exclude EA Extensions'
		- Uncheck 'Unisys/Rose Format'
		- Uncheck 'Generate Diagram Images'
3.  Export (the also written DTD can be deleted after the export)

Example screenshot:

![](export-xmi.png)